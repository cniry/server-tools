#!/bin/bash

echo "Complete artile at: http://hyperprog.com/howto/etc-git.html"

# Are we running as root?
if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root. Did you leave out sudo?"
	exit
fi

if [ -d "/etc/etc-git" ]; then
	echo "/etc/etc-git already exists! aborted."
	exit
fi

if [ -d "/etc/.git" ]; then
	echo "/etc/.git already exists! aborted."
	exit
fi


apt-get install git metastore


# Create a directory in etc to store scripts used by apt hooks
mkdir /etc/etc-git

cat <<'EOF' > /etc/etc-git/aptpreinstall.bash
#!/bin/bash

GITSTATUSOUT=`(cd /etc;git status|grep 'nothing to commit (working directory clean)')`
GITDIFFOUT=`(cd /etc;git diff)`
DATE=`date`

if test "$GITSTATUSOUT" != "nothing to commit (working directory clean)" ||
   test "$GITDIFFOUT" != ""
   then
	echo -e "\n===== /etc need git commit ====="
	(
		cd /etc/
		/usr/local/bin/eg-save
		git add --all
		git commit . -m "Automatic commit before apt-get ($DATE)"
	)
	echo -e "\n===== Comitted ====="
	exit 0
fi
exit 0
EOF


cat <<'EOF' > /etc/etc-git/aptpostinstall.bash
#!/bin/bash

GITSTATUSOUT=`(cd /etc;git status|grep 'nothing to commit (working directory clean)')`
GITDIFFOUT=`(cd /etc;git diff)`
DATE=`date`

if test "$GITSTATUSOUT" != "nothing to commit (working directory clean)" ||
   test "$GITDIFFOUT" != ""
   then
	echo -e "\n===== /etc changed need git commit ====="
	(
		cd /etc/
		/usr/local/bin/eg-save
		git add --all
		git commit . -m "Automatic commit after apt-get ($DATE)"
	)
	echo -e "\n===== Comitted ====="
	exit 0
fi
exit 0
EOF

# Setting up apt hooks to use the scripts created above.
chmod +x /etc/etc-git/*.bash
echo "DPkg::Pre-Invoke { '/etc/etc-git/aptpreinstall.bash' };" >> /etc/apt/apt.conf.d/90etc-git
echo "DPkg::Post-Invoke { '/etc/etc-git/aptpostinstall.bash' };" >> /etc/apt/apt.conf.d/90etc-git


# create empty repository
cd /etc
git init
echo "System ETC stored in GIT" > /etc/.git/description

HOSTNAME=`hostname -f`

echo -n "Enter your name for commits ex. $HOSTNAME : "
read name

echo -n "Enter your email for commits ex. localhost@$HOSTNAME : "
read email

git config --local user.name $name
git config --local user.email $email

cat <<'EOF' > /usr/local/bin/eg-save
#!/bin/sh
cd /etc
metastore -s
EOF

cat <<'EOF' > /usr/local/bin/eg-restore
#!/bin/sh
cd /etc
metastore -aeq
EOF

cat <<'EOF' > /usr/local/bin/eg-check
#!/bin/bash
CHKRES=`cd /etc;metastore -c`
if test "$CHKRES" != ""
then
	echo -e "The permission structure of the /etc is changed\n"
	echo -e "Run: eg-save"
	exit 1
fi
exit 0
EOF

cat <<'EOF' > /usr/local/bin/eg-commit
#!/bin/bash
if test "$1" == ""
then
	echo "Missing parameter!"
		echo " use: eg-commit \"Commit text\""
	exit 1
fi

cd /etc
echo "Save permissions (eg-save)..."
eg-save
echo "Add all non versioned items to git (git add --all)..."
git add --all
echo "Do commit (git commit .)..."
git commit . -m "$1"
EOF

cat <<'EOF' > /etc/.git/hooks/pre-commit
#!/bin/sh
/usr/local/bin/eg-check
EOF


#Don't forget to make this scripts executable
chmod +x /usr/local/bin/eg-*
chmod +x /etc/.git/hooks/pre-commit

#And finally put the current etc into the git repository and do the initial commit!
eg-save
git add --all
eg-commit "Initial state"


echo "Done."
